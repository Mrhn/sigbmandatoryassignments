
'''
Created on March 20, 2014

@author: Diako Mardanbegi (dima@itu.dk)
'''
from numpy import *
import numpy as np
from pylab import *
from scipy import linalg
import cv2
import cv2.cv as cv
import SIGBTools
import copy

def CalculateShadeMatrix(image, shadeRes, points, faceCorner_Normals, camera):
    #Ambient light IA=[IaR,IaG,IaB]
    IA = np.matrix([5.0, 5.0, 5.0]).T
    #Point light IA=[IpR,IpG,IpB]
    IP = np.matrix([5.0, 5.0, 5.0]).T
    #Light Source Attenuation
    fatt = 1
    #Material properties: e.g., Ka=[kaR; kaG; kaB]
    ka = np.matrix([0.2, 0.2, 0.2]).T
    kd = np.matrix([0.3, 0.3, 0.3]).T
    ks = np.matrix([0.7, 0.7, 0.7]).T
    alpha = 100

    Flat = True

    #Didn't work

    #if Flat:

    #else:

    #the surface to paint on
    surface = np.zeros((shadeRes, shadeRes, 3))

    #http://www.gameprogrammer.net/delphi3dArchive/phongfordummies.htm for inspiration

    for i in range(shadeRes):
        for j in range(shadeRes):

            #faceNormal
            fn = SIGBTools.GetFaceNormal(points)

            #A = Al*Am AKA: IA * KA(x), constant calculation of the ambient light
            ambtR = IA[0] * ka[0]
            ambtG = IA[1] * ka[1]
            ambtB = IA[2] * ka[2]

            #four points for calculating the center
            faceC = (np.array(faceCorner_Normals[:, 0]) + np.array(faceCorner_Normals[:, 1]) + np.array(faceCorner_Normals[:, 2]) + np.array(faceCorner_Normals[:, 3])) / 4

            #calculating L
            center = np.array(np.reshape(camera.center(), (1, 3)))[0]
            L = (center - faceC) / np.linalg.norm((center - faceC))

            #calculating the normals
            facecornernormals = np.zeros((shadeRes, shadeRes, 3))  #vectors have 3 variables
            pointnormals = np.zeros((shadeRes, shadeRes, 3))

            #using the interpolate hin
            IPF = np.array(SIGBTools.BilinearInterpo(shadeRes, i, j, faceCorner_Normals, True))

            #doing the max(L x N, 0)
            max = max(dot(IPF, L),0)

            #then combining them into D = Dl*Dm*max(L x N, 0)
            diffuR = IP[0] * kd[0] * max
            diffuG = IP[1] * kd[1] * max
            diffuB = IP[2] * kd[2] * max

            #Specular S = Sl*Sm*max((R x V), 0)n

            #calculating V
            V = camera.center() - faceC
            V /= np.linalg.norm(V)

            #Calculating R  2(N x L)N - L
            R = 2 * np.dot(L, fn) * fn - L

            #calculating the max of S
            spect = np.power(max(np.dot(R, V).tolist(), 0), alpha)

            #combining it into S = Sl*Sm*max((R x V), 0)n
            specuR =  IP[0] * ks[0] * spect
            specuG = IP[1] * ks[1] * spect
            specuB = IP[2] * ks[2] * spect

            surface[i][j] = ambtR + diffuR + specuR, ambtG + diffuG + specuG, ambtB + diffuB + specuB

    return surface[:, :, 0], surface[:, :, 1], surface[:, :, 2]

def ShadeFace(image, points, faceCorner_Normals, camera):
    global shadeRes
    shadeRes = 10
    videoHeight, videoWidth, vd = array(image).shape
    #................................
    points_Proj = camera.project(SIGBTools.toHomogenious(points))
    points_Proj1 = np.array([[int(points_Proj[0, 0]), int(points_Proj[1, 0])], [int(
        points_Proj[0, 1]), int(points_Proj[1, 1])], [int(points_Proj[0, 2]), int(points_Proj
    [1, 2])], [int(points_Proj[0, 3]), int(points_Proj[1, 3])]])
    #................................
    square = np.array([[0, 0], [shadeRes - 1, 0], [shadeRes - 1, shadeRes - 1], [0, shadeRes - 1]])
    #................................
    H = SIGBTools.estimateHomography(square, points_Proj1)
    #................................
    Mr0, Mg0, Mb0 = CalculateShadeMatrix(image, shadeRes, points, faceCorner_Normals, camera)
    # HINT
    # type(Mr0): <type 'numpy.ndarray'>
    # Mr0.shape: (shadeRes, shadeRes)
    #...............................
    Mr = cv2.warpPerspective(Mr0, H, (videoWidth, videoHeight), flags=cv2.INTER_LINEAR)
    Mg = cv2.warpPerspective(Mg0, H, (videoWidth, videoHeight), flags=cv2.INTER_LINEAR)
    Mb = cv2.warpPerspective(Mb0, H, (videoWidth, videoHeight), flags=cv2.INTER_LINEAR)
    #................................
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    [r, g, b] = cv2.split(image)
    #................................
    whiteMask = np.copy(r)
    whiteMask[:, :] = [0]
    points_Proj2 = []
    points_Proj2.append([int(points_Proj[0, 0]), int(points_Proj[1, 0])])
    points_Proj2.append([int(points_Proj[0, 1]), int(points_Proj[1, 1])])
    points_Proj2.append([int(points_Proj[0, 2]), int(points_Proj[1, 2])])
    points_Proj2.append([int(points_Proj[0, 3]), int(points_Proj[1, 3])])
    cv2.fillConvexPoly(whiteMask, array(points_Proj2), (255, 255, 255))
    #................................
    r[np.nonzero(whiteMask > 0)] = map(lambda x: max(min(x, 255), 0), r[nonzero(whiteMask > 0)] * Mr[
        np.nonzero(whiteMask > 0)])
    g[nonzero(whiteMask > 0)] = map(lambda x: max(min(x, 255), 0), g[nonzero(whiteMask > 0)] * Mg[
        nonzero(whiteMask > 0)])
    b[nonzero(whiteMask > 0)] = map(lambda x: max(min(x, 255), 0), b[nonzero(whiteMask > 0)] * Mb[
        nonzero(whiteMask > 0)])
    #................................
    image = cv2.merge((r, g, b))
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    return image

def TextureFace(i, tf, cam, f):
    tex = cv2.imread(f)
    x, y, z = tex.shape
    mask = zeros((x, y)) + 255

    texture_points = np.array([[0.0, 0.0],
                               [float(y), 0.0],
                               [float(y), float(x)],
                               [0.0, float(x)]])

    tmp = np.column_stack((tf.T, np.ones((tf.T.shape[0], 1)))).T
    H = cv2.findHomography(texture_points, cam.project(tmp).T[:, :-1])[0] #compared the texture to projection

    #warp texture and mask into place
    warped_tex = cv2.warpPerspective(tex, H, (i.shape[1], i.shape[0]))
    warped_mask = cv2.warpPerspective(mask, H, (i.shape[1], i.shape[0]))

    #choose what part of the mask to draw on
    toDraw = warped_mask != 0
    i[toDraw] = warped_tex[toDraw]

    return i

def DrawLines(img, points):
    for i in range(1, 17):
         x1 = points[0, i - 1]
         y1 = points[1, i - 1]
         x2 = points[0, i]
         y2 = points[1, i]
         cv2.line(img, (int(x1), int(y1)), (int(x2), int(y2)), (255, 0, 0),5)
    return img

def update(img):
    image=copy.copy(img)


    if Undistorting:  #Use previous stored camera matrix and distortion coefficient to undistort the image
        ''' <004> Here Undistoret the image'''
        image = cv2.undistort(image, cameraMatrix, distortionCoefficient)

    if (ProcessFrame):

        ''' <005> Here Find the Chess pattern in the current frame'''
        patternFound, corners = cv2.findChessboardCorners(cv2.cvtColor(image,cv2.COLOR_BGR2GRAY),
        (9,6), flags =  cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE
        + cv2.CALIB_CB_FAST_CHECK);


        if patternFound ==True:

            ''' <006> Here Define the cameraMatrix P=K[R|t] of the current frame'''

            #redudant code, but works, paints the corners
            points = [0,8,45,53]
            oc = []
            for i in points:
                oc.append([int(corners[i, 0, 0]),int(corners[i, 0, 1])])

            for p in oc:
                cv2.circle(image,(p[0], p[1]), 2,(255,0,0),2)

            h21 = calculateHomography(np.array(oc), findOuterPoints(calibrationPoints[0]))
            h2cs = np.dot(h1cs[0], h21[0])

            #example from slide, creates new camera projection
            cam2 = SIGBTools.Camera(np.dot(h2cs,camera.P))
            A = np.dot(linalg.inv(cameraMatrix),cam2.P[:,:3])

            #normalize to avoid it from distorting
            norm1 = np.cross(A[:,0],A[:,1]).T
            norm1 = norm1 / np.linalg.norm(norm1)
            norm2 = A[:,1]
            norm2 = norm2 / np.linalg.norm(norm2)
            norm3 = A[:,0]
            norm3 = norm3 / np.linalg.norm(norm3)

            A = np.array([norm3,norm2,norm1]).T

            cam2.P[:,:3] = np.dot(cameraMatrix,A)

            if MethodX:
                projection = cam2.P
            else:
                found,rvecs_new,tvecs_new = cv2.solvePnP(calibrationPoints[0], corners, cameraMatrix, distortionCoefficient)
                projection = calculateCameraMatrix(cameraMatrix, rvecs_new, tvecs_new)

            cam2 = SIGBTools.Camera(projection)

            #Reference Trouble
            cam3 = copy.deepcopy(cam2)
            if ShowText:
                ''' <011> Here show the distance between the camera origin and the world origin in the image'''

                cam3.factor()
                center2 = cam3.center()
                distance2 = np.sqrt(pow(center2[0], 2) + np.power(center2[1], 2) + np.power(center2[2], 2))

                cv2.putText(image, str("distance to center:" + str(distance2)), (20, 30), cv2.FONT_HERSHEY_PLAIN, 1,
                            (255, 255, 255))
                cv2.putText(image, str("frame:" + str(frameNumber)), (20, 10), cv2.FONT_HERSHEY_PLAIN, 1,
                            (255, 255, 255))

            ''' <008> Here Draw the world coordinate system in the image'''
            c = [0., 0., 0.]
            x = [2.0, 0., 0.]
            y = [0., 2.0, 0.]
            z = [0., 0., -2.0]
            X = np.array([c,x,y,z])

            tmp = cam2.project(np.column_stack((X,np.ones((X.shape[0],1)))).T).T

            cv2.line(image, (int(tmp[0][0]),int(tmp[0][1])), (int(tmp[1][0]),int(tmp[1][1])),(0, 0, 255),2)
            cv2.line(image, (int(tmp[0][0]),int(tmp[0][1])), (int(tmp[2][0]),int(tmp[2][1])), (0, 0, 255),2)
            cv2.line(image, (int(tmp[0][0]),int(tmp[0][1])), (int(tmp[3][0]),int(tmp[3][1])), (0, 0, 255),2)

            cv2.circle(image, (int(tmp[1][0]),int(tmp[0][1])), 3, (0, 255, 0), 1)
            cv2.circle(image, (int(tmp[1][0]),int(tmp[1][1])), 3, (0, 255, 0), 1)
            cv2.circle(image, (int(tmp[2][0]),int(tmp[2][1])), 3, (0, 255, 0), 1)
            cv2.circle(image, (int(tmp[3][0]),int(tmp[3][1])), 3, (0, 255, 0), 1)

            if TextureMap:

                ''' <010> Here Do he texture mapping and draw the texture on the faces of the cube'''

                TopFaceCornerNormals, RightFaceCornerNormals, LeftFaceCornerNormals, UpFaceCornerNormals, DownFaceCornerNormals = SIGBTools.CalculateFaceCornerNormals(TopFace, RightFace, LeftFace, UpFace, DownFace)

                cornernormals = [UpFaceCornerNormals, DownFaceCornerNormals, LeftFaceCornerNormals, RightFaceCornerNormals, TopFaceCornerNormals]
                faces = ['Images/Up.jpg', 'Images/Down.jpg', 'Images/Left.jpg', 'Images/Right.jpg', 'Images/Top.jpg']
                boxFaces = [UpFace, DownFace, LeftFace, RightFace, TopFace]

                ''' <012>  calculate the normal vectors of the cube faces and draw these normal vectors on the center of each face'''

                for i in range(len(boxFaces)):
                    fn = SIGBTools.GetFaceNormal(boxFaces[i])

                    faceC = (np.array(boxFaces[i][:, 0])+ np.array(boxFaces[i][:, 1])+ np.array(boxFaces[i][:, 2])+ np.array(boxFaces[i][:, 3])) / 4
                    faceEnd = cam2.project(SIGBTools.toHomogenious(np.reshape(faceC, (3, 1))))
                    end = cam2.project(SIGBTools.toHomogenious(np.reshape(fn + faceC, (3, 1))))

                    ''' <013> Here Remove the hidden faces'''

                    cameraVector = faceC - cam3.center()
                    cameraVector /= np.linalg.norm(cameraVector)

                    a = np.degrees(np.arccos(dot(fn, cameraVector))).tolist() #numpy hack tolist

                    b = True

                    #remove faces if you cant see em
                    for angle in a[0]:
                        if angle <= 90.0:
                            b = False

                    #only draw remaining faces
                    if b:
                        image = TextureFace(image, boxFaces[i], cam2, faces[i])
                        cv2.line(image, (faceEnd[0], faceEnd[1]), (end[0], end[1]), (255, 0, 0), 4)

                    #not working
                    #image = ShadeFace(image, boxFaces[i], cornernormals[i], cam2)

            if ProjectPattern:
                ''' <007> Here Test the camera matrix of the current view by projecting the pattern points'''
                for p in corners:
                    cv2.circle(image,(p[0][0], p[0][1]), 2,(255,0,0),2)

            if WireFrame:
                ''' <009> Here Project the box into the current camera image and draw the box edges'''
                image = DrawLines(image, cam2.project(SIGBTools.toHomogenious(box)))


    cv2.namedWindow('Web cam')
    cv2.imshow('Web cam', image)
    global result
    result=copy.copy(image)

def getImageSequence(capture, fastForward):
    '''Load the video sequence (fileName) and proceeds, fastForward number of frames.'''
    global frameNumber

    for t in range(fastForward):
        isSequenceOK, originalImage = capture.read()  # Get the first frames
        frameNumber = frameNumber+1
    return originalImage, isSequenceOK


def printUsage():
    print "Q or ESC: Stop"
    print "SPACE: Pause"
    print "p: turning the processing on/off "
    print 'u: undistorting the image'
    print 'g: project the pattern using the camera matrix (test)'
    print 'x: your key!'

    print 'the following keys will be used in the next assignment'
    print 'i: show info'
    print 't: texture map'
    print 's: save frame'



def run(speed,video):

    '''MAIN Method to load the image sequence and handle user inputs'''

    #--------------------------------video
    capture = cv2.VideoCapture(video)


    image, isSequenceOK = getImageSequence(capture,speed)

    if(isSequenceOK):
        update(image)
        printUsage()

    while(isSequenceOK):
        OriginalImage=copy.copy(image)


        inputKey = cv2.waitKey(1)

        if inputKey == 32:#  stop by SPACE key
            update(OriginalImage)
            if speed==0:
                speed = tempSpeed;
            else:
                tempSpeed=speed
                speed = 0;

        if (inputKey == 27) or (inputKey == ord('q')):#  break by ECS key
            break

        if inputKey == ord('p') or inputKey == ord('P'):
            global ProcessFrame
            if ProcessFrame:
                ProcessFrame = False;

            else:
                ProcessFrame = True;
            update(OriginalImage)

        if inputKey == ord('u') or inputKey == ord('U'):
            global Undistorting
            if Undistorting:
                Undistorting = False;
            else:
                Undistorting = True;
            update(OriginalImage)
        if inputKey == ord('w') or inputKey == ord('W'):
            global WireFrame
            if WireFrame:
                WireFrame = False;

            else:
                WireFrame = True;
            update(OriginalImage)

        if inputKey == ord('x') or inputKey == ord('X'):
            global MethodX
            if MethodX:
                MethodX= False;
            else:
                MethodX = True;
            update(OriginalImage)

        if inputKey == ord('i') or inputKey == ord('I'):
            global ShowText
            if ShowText:
                ShowText = False;

            else:
                ShowText = True;
            update(OriginalImage)

        if inputKey == ord('t') or inputKey == ord('T'):
            global TextureMap
            if TextureMap:
                TextureMap = False;

            else:
                TextureMap = True;
            update(OriginalImage)

        if inputKey == ord('g') or inputKey == ord('G'):
            global ProjectPattern
            if ProjectPattern:
                ProjectPattern = False;

            else:
                ProjectPattern = True;
            update(OriginalImage)

        if inputKey == ord('x') or inputKey == ord('X'):
            global debug
            if debug:
                debug = False;
            else:
                debug = True;
            update(OriginalImage)


        if inputKey == ord('s') or inputKey == ord('S'):
            name='Saved Images/Frame_' + str(frameNumber)+'.png'
            cv2.imwrite(name,result)

        if (speed>0):
            update(image)
            image, isSequenceOK = getImageSequence(capture,speed)



def getNumpyData():
    global cameraMatrix
    global distortionCoefficient
    global homographyPoints
    global calibrationPoints
    global calibrationCamera
    global chessSquare_size
    global rotationVectors
    global translationVectors

    cameraMatrix = np.load('numpyData/camera_matrix.npy')
    distortionCoefficient = np.load('numpyData/distortionCoefficient.npy')
    rotationVectors = np.load('numpyData/rotatioVectors.npy')
    translationVectors = np.load('numpyData/translationVectors.npy')
    calibrationPoints = np.load('numpyData/obj_points.npy')
    print "numpy data start"
    print cameraMatrix
    print distortionCoefficient
    print rotationVectors
    print translationVectors
    print calibrationPoints
    print "numpy data slut"

def calculateCameraMatrix(k, r,t ):
    return np.dot(k, np.hstack((cv2.Rodrigues(r)[0],t))) # using hint


'''-------------------MAIN BODY--------------------------------------------------------------------'''
'''--------------------------------------------------------------------------------------------------------------'''

'''-------variables------'''
global cameraMatrix
global distortionCoefficient
global homographyPoints
global calibrationPoints
global calibrationCamera
global chessSquare_size
global rotationVectors
global translationVectors

ProcessFrame=False
Undistorting=False
WireFrame=False
MethodX=False
ShowText=True
TextureMap=True
ProjectPattern=False
debug=True

tempSpeed=1
frameNumber=0
chessSquare_size=2



'''-------defining the cube------'''

box = SIGBTools.getCubePoints([4, 2.5, 0], 1,chessSquare_size)


i = array([ [0,0,0,0],[1,1,1,1] ,[2,2,2,2]  ])  # indices for the first dim
j = array([ [0,3,2,1],[0,3,2,1] ,[0,3,2,1]  ])  # indices for the second dim
TopFace = box[i,j]


i = array([ [0,0,0,0],[1,1,1,1] ,[2,2,2,2]  ])  # indices for the first dim
j = array([ [3,8,7,2],[3,8,7,2] ,[3,8,7,2]  ])  # indices for the second dim
RightFace = box[i,j]


i = array([ [0,0,0,0],[1,1,1,1] ,[2,2,2,2]  ])  # indices for the first dim
j = array([ [5,0,1,6],[5,0,1,6] ,[5,0,1,6]  ])  # indices for the second dim
LeftFace = box[i,j]


i = array([ [0,0,0,0],[1,1,1,1] ,[2,2,2,2]  ])  # indices for the first dim
j = array([ [5,8,3,0], [5,8,3,0] , [5,8,3,0] ])  # indices for the second dim
UpFace = box[i,j]


i = array([ [0,0,0,0],[1,1,1,1] ,[2,2,2,2]  ])  # indices for the first dim
j = array([ [1,2,7,6], [1,2,7,6], [1,2,7,6] ])  # indices for the second dim
DownFace = box[i,j]

def projectPoints(camera, points):
    return camera.project(np.vstack((points.T, np.ones(points.T.shape[1])))).T

def testDrawPoints(camera, points):
    img = cv2.imread('01.png')

    for p in points:
        C = int(p[0]),int(p[1])
        cv2.circle(img,C, 2,(255,0,0),2)

    cv2.imshow('tmpImg',img)

def calculateHomography(points1, points2):
    return cv2.findHomography(points1.astype(float32), points2.astype(float32))

def findOuterPoints(corners, removeRow = True):
    a = np.array([corners[0], corners[8], corners[45], corners[53]])
    if removeRow:
        return np.delete(a,-1,1)
    else:
        return a

'''----------------------------------------'''
'''----------------------------------------'''

SIGBTools.calibrateCamera(n = 5,fileName = 0)

''' <001> Here Load the numpy data files saved by the cameraCalibrate2'''

getNumpyData()

''' <002> Here Define the camera matrix of the first view image (01.png) recorded by the cameraCalibrate2'''

P1 = calculateCameraMatrix(cameraMatrix,rotationVectors[0],translationVectors[0])
camera = SIGBTools.Camera(P1)

''' <003> Here Load the first view image (01.png) and find the chess pattern and store the 4 corners of the pattern needed for homography estimation'''

projectedPoints = projectPoints(camera,calibrationPoints[0])

testDrawPoints(camera, projectedPoints)

h1cs = calculateHomography(findOuterPoints(calibrationPoints[0]), findOuterPoints(projectedPoints))

SIGBTools.RecordVideoFromCamera()

run(1,0)


#from scipy import ndimage
import cv2
import numpy as np
from pylab import *
from matplotlib import *
from matplotlib.pyplot import *
from scipy import *
import math
import SIGBTools

def frameTrackingData2BoxData(data):
    #Convert a row of points into tuple of points for each rectangle
    pts= [ (int(data[i]),int(data[i+1])) for i in range(0,11,2) ]
    boxes = [];
    for i in range(0,7,2):
        box = tuple(pts[i:i+2])
        boxes.append(box)   
    return boxes

def simpleTextureMap():

    I1 = cv2.imread('Images/ITULogo.jpg')
    I2 = cv2.imread('Images/ITUMap.bmp')

    #Print Help
    H,Points  = SIGBTools.getHomographyFromMouse(I1,I2,4)
    h, w,d = I2.shape
    overlay = cv2.warpPerspective(I1, H, (w, h))
    M = cv2.addWeighted(I2, 0.5, overlay, 0.5, 0)

    cv2.imshow("Overlayed Image",M)
    cv2.waitKey(0)

def texturemapGroundFloor():
    I1 = cv2.imread('Images/ITULogo.jpg')
    I2 = cv2.imread('Images/ITUMap.bmp')

    # Setting alst value to <0, only have to mark points in I2 (4 points)
    H,Points  = SIGBTools.getHomographyFromMouse(I1,I2,-4)
    h, w,d = I2.shape
    overlay = cv2.warpPerspective(I1, H, (w, h))
    M = cv2.addWeighted(I2, 0.5, overlay, 0.5, 0)

    cv2.imshow("Overlayed Image",M)
    cv2.waitKey(0)

def showImageandPlot(N):
    #A simple attenmpt to get mouse inputs and display images using matplotlib
    I = cv2.imread('groundfloor.bmp')
    drawI = I.copy()
    #make figure and two subplots
    fig = figure(1) 
    ax1  = subplot(1,2,1) 
    ax2  = subplot(1,2,2) 
    ax1.imshow(I) 
    ax2.imshow(drawI)
    ax1.axis('image') 
    ax1.axis('off') 
    points = fig.ginput(5) 
    fig.hold('on')
    
    for p in points:
        #Draw on figure
        subplot(1,2,1)
        plot(p[0],p[1],'rx')
        #Draw in image
        cv2.circle(drawI,(int(p[0]),int(p[1])),2,(0,255,0),10)
    ax2.cla
    ax2.imshow(drawI)
    draw() #update display: updates are usually defered 
    show()
    savefig('somefig.jpg')
    cv2.imwrite("drawImage.jpg", drawI)

def texturemapGridSequence():
    """ Skeleton for texturemapping on a video sequence"""
    fn = 'GridVideos/grid4.mp4'
    cap = cv2.VideoCapture(fn)
    drawContours = True;

    texture = cv2.imread('Images/ITULogo.jpg')
    texture = cv2.pyrDown(texture)

    mTex,nTex,t = texture.shape

    texCorners = np.array([[0,0], [0,nTex], [mTex,0], [mTex,nTex]])

    #load Tracking data
    running, imgOrig = cap.read()

    #cv2.imshow("win2",imgOrig)

    pattern_size = (9, 6)

    idx = [0,8,45,53] # The index of the outer corners
    while(running):
    #load Tracking data
        running, imgOrig = cap.read()
        if(running):
            imgOrig = cv2.pyrDown(imgOrig)
            gray = cv2.cvtColor(imgOrig,cv2.COLOR_BGR2GRAY)
            mI,nI = gray.shape
            found, corners = cv2.findChessboardCorners(gray, pattern_size)

            if found:
                term = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1 )
                cv2.cornerSubPix(gray, corners, (5, 5), (-1, -1), term)

                corners2 = np.array([corners[0][0], corners[8][0], corners[45][0], corners[53][0]])

                H = SIGBTools.estimateHomography(texCorners, corners2)

                '''
                corners2 = array([corners[0][0], corners[8][0], corners[-9][0], corners[-1][0]])
                H = SIGBTools.estimateHomography(texCorners, corners2)
                '''

                overlay = cv2.warpPerspective(texture, H, (nI, mI))
                M = cv2.addWeighted(imgOrig, 0.5, overlay, 0.5, 0)

                ''' unused code
                for t in idx:
                    cv2.circle(imgOrig,(int(corners[t,0,0]),int(corners[t,0,1])),10,(255,t,t))
                '''

                cv2.imshow("win2", M)
            cv2.waitKey(1)

def realisticTexturemap(scale, map):
    G = cv2.imread('groundfloor.jpg')
    g_h, g_w, g_d = G.shape

    H_M_G = np.load('homo.npy')
    H_G_M = np.linalg.inv(H_M_G)
    # TODO: Might be normalized

    texture = cv2.imread('Images/ITULogo.jpg')
    h, w, d = texture.shape

    texCorners = np.array([[0,0], [0,h], [w,h], [w,0]])

    h_scaled = h*scale;
    w_scaled = w*scale;

    fig = figure(1)
    ax = subplot(1, 1, 1)
    ax.imshow(map)
    #ax.axis('')
    title("Click in the image")
    fig.canvas.draw()
    ax.hold('On')
    x, y = fig.ginput(1, -1)[0]

    map_points = np.array( [[x,            y],
                            [x,            y + h_scaled],
                            [x + w_scaled, y + h_scaled],
                            [x + w_scaled, y]])

    H_M_T = SIGBTools.estimateHomography(texCorners, map_points)

    #H_M_T, p = SIGBTools.getHomographyFromMouse(texture, map, -4)

    H_G_T = np.dot(H_G_M, H_M_T) # H_G_M ER FEJLEN!

    overlay = cv2.warpPerspective(texture, H_G_T, (g_w, g_h))
    M = cv2.addWeighted(G, 0.5, overlay, 0.5, 0)

    cv2.imshow("map", M)
    cv2.waitKey(0)

def display_trace(H, foot_box, plan, cleanPlan):
    if H == "load":
        H = numpy.load("homo.npy")
    else:
        numpy.save("homo", H)

    livePlan = np.copy(cleanPlan)

    # Calc the center of the bottom of the feet (min Y, between the two Xs)
    x1,y1 = foot_box[0]
    x2,y2 = foot_box[1]
    x = min(x1,x2) - (math.fabs(x1-x2)/2)
    y = min(y1, y2)

    xy1 = array([[x],[y],[1]])
    xy1_new = dot(H, xy1)

    xn = xy1_new[0]/xy1_new[2]
    yn = xy1_new[1]/xy1_new[2]

    cv2.circle(plan, (int(xn), int(yn)), 1, (255, 0, 255), 2)
    cv2.circle(livePlan, (int(xn), int(yn)), 5, (255, 0, 255), 2)
    cv2.imshow("Map", livePlan)
    cv2.imwrite("pathResult.png", plan)

def showFloorTrackingData():
    #Load videodata
    fn = "GroundFloorData/sunclipds.avi"
    cap = cv2.VideoCapture(fn)
    
    #load Tracking data
    running, imgOrig = cap.read()

    plan = cv2.imread("Images/ITUMap.bmp")
    livePlan = np.copy(plan)

    H_gm, mousePoints = SIGBTools.getHomographyFromMouse(imgOrig,plan)

   # print H_gm

    dataFile = np.loadtxt('GroundFloorData/trackingdata.dat')
    m,n = dataFile.shape
    
    fig = figure()
    for k in range(m):
        running, imgOrig = cap.read() 
        if(running):
            boxes = frameTrackingData2BoxData(dataFile[k,:])
            display_trace(H_gm, boxes[1], plan, livePlan)
            boxColors = [(255,0,0),(0,255,0),(0,0,255)]
            for k in range(0,3):
                aBox = boxes[k]
                cv2.rectangle(imgOrig, aBox[0], aBox[1], boxColors[k])
            cv2.imshow("boxes",imgOrig)
            cv2.waitKey(1)

def angle_cos(p0, p1, p2):
    d1, d2 = p0-p1, p2-p1
    return abs( np.dot(d1, d2) / np.sqrt( np.dot(d1, d1)*np.dot(d2, d2) ) )

def findSquares(img,minSize = 2000,maxAngle = 1):
    """ findSquares intend to locate rectangle in the image of minimum area, minSize, and maximum angle, maxAngle, between 
    sides"""
    squares = []
    contours, hierarchy = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
         cnt_len = cv2.arcLength(cnt, True)
         cnt = cv2.approxPolyDP(cnt, 0.08*cnt_len, True)
         if len(cnt) == 4 and cv2.contourArea(cnt) > minSize and cv2.isContourConvex(cnt):
             cnt = cnt.reshape(-1, 2)
             max_cos = np.max([angle_cos( cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4] ) for i in xrange(4)])
             if max_cos < maxAngle:
                 squares.append(cnt)
    return squares

def DetectPlaneObject(I,minSize=1000):
      """ A simple attempt to detect rectangular 
      color regions in the image"""
      HSV = cv2.cvtColor(I, cv2.COLOR_BGR2HSV)
      h = HSV[:,:,0].astype('uint8')
      s = HSV[:,:,1].astype('uint8')
      v = HSV[:,:,2].astype('uint8')
      
      b = I[:,:,0].astype('uint8')
      g = I[:,:,1].astype('uint8')
      r = I[:,:,2].astype('uint8')
     
      # use red channel for detection.
      s = (255*(r>230)).astype('uint8')
      iShow = cv2.cvtColor(s, cv2.COLOR_GRAY2BGR)
      cv2.imshow('ColorDetection',iShow)
      squares = findSquares(s,minSize)
      return squares
  
def texturemapObjectSequence():
    """ Poor implementation of simple texturemap """
    fn = 'BookVideos/Seq1_scene.mp4'
    cap = cv2.VideoCapture(fn) 
    drawContours = True;
    
    texture = cv2.imread('images/ITULogo.jpg')
    #texture = cv2.transpose(texture)
    mTex,nTex,t = texture.shape
    
    #load Tracking data
    running, imgOrig = cap.read()
    mI,nI,t = imgOrig.shape

    print running 
    while(running):
        for t in range(20):
            running, imgOrig = cap.read() 
        
        if(running):
            squares = DetectPlaneObject(imgOrig)
            
            for sqr in squares:
                 #Do texturemap here!!!!
                 #TODO
                 
                 if(drawContours):                
                     for p in sqr:
                         cv2.circle(imgOrig,(int(p[0]),int(p[1])),3,(255,0,0)) 
                 
            
            if(drawContours and len(squares)>0):    
                cv2.drawContours( imgOrig, squares, -1, (0, 255, 0), 3 )

            cv2.circle(imgOrig,(100,100),10,(255,0,0))
            cv2.imshow("Detection",imgOrig)
            cv2.waitKey(1)

#showFloorTrackingData()
#simpleTextureMap()
#texturemapGroundFloor()

I2 = cv2.imread('Images/ITUMap.bmp')
#realisticTexturemap(0.25,I2)
texturemapGridSequence()
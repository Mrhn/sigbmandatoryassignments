# -*- coding: utf-8 -*-
#----------------
from SIGBTools import *
from scipy.cluster.vq import *
from matplotlib.pyplot import *
import numpy as np
import scipy as sp

inputFile = "Sequences/eye1.mp4"
outputFile = "eyeTrackerResult.mp4"

#--------------------------
#         Global variable
#--------------------------
global imgOrig, leftTemplate, rightTemplate, frameNr
imgOrig = []
#These are used for template matching
leftTemplate = None
rightTemplate = None
frameNr = 0

#Methods
def GetPupil(gray, thr, minSize, maxSize, minExtend, maxExtend, minCircle, maxCircle, morphStr):
    #Given a gray level image, gray and threshold value return a list of pupil locations#
    tempResultImg = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
    props = RegionProps()

    val, binI = cv2.threshold(gray, thr, 255, cv2.THRESH_BINARY_INV)

    st = cv2.getStructuringElement(cv2.MORPH_CROSS, (morphStr, morphStr))
    binI = cv2.morphologyEx(binI, cv2.MORPH_DILATE, st)
    binI = cv2.morphologyEx(binI, cv2.MORPH_ERODE, st)

    cv2.imshow("Threshold", binI)

    contours, hierarchy = cv2.findContours(binI, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    pupils = []

    for contour in contours:
        prop = props.CalcContourProperties(contour, ['centroid', 'area', 'extend', 'perimiter'])
        center, area, extend, circleProx = prop.get("Centroid"), prop.get('Area'), prop.get("Extend"), (
            prop.get("Perimiter") / (2 * np.sqrt(np.pi * prop.get('Area'))))  #One liners ftw!

        if minSize < area < maxSize and minExtend < extend < maxExtend and minCircle < circleProx < maxCircle:  # Filtering by the contour properties
            ellipse = cv2.fitEllipse(contour)
            pupils.append(ellipse)
            cv2.circle(tempResultImg, (int(ellipse[0][0]), int(ellipse[0][1])), 2, (255, 0, 255), 5)

    cv2.imshow("TempResults", tempResultImg)
    return pupils


def GetGlints(gray, thr, minSize, maxSize, morphStr):
    tempResultImg = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

    props = RegionProps()

    val, binI = cv2.threshold(gray, thr, 255, cv2.THRESH_BINARY)

    st = cv2.getStructuringElement(cv2.MORPH_CROSS, (morphStr, morphStr))
    binI = cv2.morphologyEx(binI, cv2.MORPH_DILATE, st)

    cv2.imshow("Glint", binI)

    contours, hierarchy = cv2.findContours(binI, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    glints = []

    for contour in contours:
        prop = props.CalcContourProperties(contour, ['centroid', 'area', 'extend'])
        center, area, extend = (int(prop.get("Centroid")[0]), int(prop.get("Centroid")[1])), prop.get('Area'), prop.get(
            "Extend")

        if (minSize < area < maxSize):  # Found a pupil
            glints.append([center[0], center[1]])
            cv2.circle(tempResultImg, (center[0], center[1]), 2, (255, 0, 255), 5)

    cv2.imshow("TempResults", tempResultImg)
    return glints


def GetIrisUsingThreshold(gray, thr):
    ''' Given a gray level image, gray and threshold
    value return a list of iris locations'''
    # YOUR IMPLEMENTATION HERE !!!!

    props = RegionProps()

    val, binI = cv2.threshold(gray, thr, 255, cv2.THRESH_BINARY_INV)

    st = cv2.getStructuringElement(cv2.MORPH_CROSS, (10, 10))
    binI = cv2.morphologyEx(binI, cv2.MORPH_DILATE, st)
    binI = cv2.morphologyEx(binI, cv2.MORPH_ERODE, st)

    contours, hierarchy = cv2.findContours(binI, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    print '--iris--'
    for contour in contours:
        prop = props.CalcContourProperties(contour, ['centroid', 'area', 'extend', 'perimiter'])
        center = prop.get("Centroid")
        center = (int(center[0]), int(center[1]))
        area = prop.get('Area')
        extend = prop.get("Extend")
        perimiter = prop.get("Perimiter")
        circleProx = perimiter / (2 * np.sqrt(np.pi * area))

        #if 0.95 < circleProx < 1.65:
            #ellipse = cv2.fitEllipse(contour)
            #cv2.ellipse(gray, ellipse, (0, 255, 0), 1)

    cv2.imshow("Iris", binI)


def circularHough(gray):
    ''' Performs a circular hough transform of the image, gray and shows the  detected circles
    The circe with most votes is shown in red and the rest in green colors '''
    #See help for http://opencv.itseez.com/modules/imgproc/doc/feature_detection.html?highlight=houghcircle#cv2.HoughCircles
    blur = cv2.GaussianBlur(gray, (31, 31), 11)

    dp = 6;
    minDist = 30
    highThr = 20  #High threshold for canny
    accThr = 850;  #accumulator threshold for the circle centers at the detection stage. The smaller it is, the more false circles may be detected
    maxRadius = 50;
    minRadius = 155;
    circles = cv2.HoughCircles(blur, cv2.cv.CV_HOUGH_GRADIENT, dp, minDist, None, highThr, accThr, maxRadius, minRadius)

    #Make a color image from gray for display purposes
    gColor = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
    if (circles != None):
        #print circles
        all_circles = circles[0]
        M, N = all_circles.shape
        k = 1
        for c in all_circles:
            cv2.circle(gColor, (int(c[0]), int(c[1])), c[2], (int(k * 255 / M), k * 128, 0))
            K = k + 1
            # Sure it is not "k" (small-k) ?
        c = all_circles[0, :]
        cv2.circle(gColor, (int(c[0]), int(c[1])), c[2], (0, 0, 255), 5)
        cv2.imshow("hough", gColor)


def GetIrisUsingNormals(gray, pupil, normalLength):
    ''' Given a gray level image, gray and the length of the normals, normalLength
     return a list of iris locations'''
    # YOUR IMPLEMENTATION HERE !!!!
    pass


def GetIrisUsingSimplifyedHough(gray, pupil):
    ''' Given a gray level image, gray
    return a list of iris locations using a simplified Hough transformation'''
    # YOUR IMPLEMENTATION HERE !!!!
    pass


def GetEyeCorners(leftTemplate, rightTemplate, pupilPosition=None):
    eyecorners = []
    try:
        if leftTemplate != None and rightTemplate != None and pupilPosition != None:
            tmpImgLeft = None
            tmpImgRight = None

            if pupilPosition != None:
                hOrg, wOrg = imgOrig.shape[:2]
                tmpImgLeft = imgOrig[0:hOrg, 0:pupilPosition[0]]
                tmpImgRight = imgOrig[0:hOrg, pupilPosition[0]:wOrg]

            w, h = leftTemplate.shape[:2]

            leftEye = cv2.matchTemplate(tmpImgLeft, leftTemplate, cv2.TM_CCORR_NORMED)
            (minVal, maxVal, Pmin, Pmax) = cv2.minMaxLoc(leftEye)
            eyecorners.append([Pmax[0], Pmax[1], Pmax[0] + h, Pmax[1] + w])

            w, h = rightTemplate.shape[:2]
            rightEye = cv2.matchTemplate(tmpImgRight, rightTemplate, cv2.TM_CCORR_NORMED)
            (minVal, maxVal, Pmin, Pmax) = cv2.minMaxLoc(rightEye)
            eyecorners.append([Pmax[0] + pupilPosition[0], Pmax[1], Pmax[0] + h + pupilPosition[0], Pmax[1] + w])
        elif leftTemplate != None and rightTemplate != None:
            w, h = leftTemplate.shape[:2]

            leftEye = cv2.matchTemplate(imgOrig, leftTemplate, cv2.TM_CCORR_NORMED)
            (minVal, maxVal, Pmin, Pmax) = cv2.minMaxLoc(leftEye)
            eyecorners.append([Pmax[0], Pmax[1], Pmax[0] + h, Pmax[1] + w])

            w, h = rightTemplate.shape[:2]
            rightEye = cv2.matchTemplate(imgOrig, rightTemplate, cv2.TM_CCORR_NORMED)
            (minVal, maxVal, Pmin, Pmax) = cv2.minMaxLoc(rightEye)
            eyecorners.append([Pmax[0], Pmax[1], Pmax[0] + h, Pmax[1] + w])
        return eyecorners
    except:
        print "error"
        return eyecorners



def distancepoints(p1, p2):
    return math.hypot(p2[0] - p1[0], p2[1] - p1[1])


def FilterPupilGlint(pupils, glints, minGlintPupilDistance, maxGlintPupilDistance):
    result = []
    for pupil in pupils:
        for glint in glints:
            p1 = int(pupil[0][0]), int(pupil[0][1])
            p2 = int(glint[0]), int(glint[1])

            if minGlintPupilDistance < distancepoints(p1, p2) < maxGlintPupilDistance:
                result.append(glint)

    return result

def getGradientImageInfo(I):
    scale = 1
    delta = 0
    ddepth = cv2.cv.CV_32F
    k = 3

    grad_x = cv2.Sobel(I, ddepth, 1, 0, ksize=k, scale=scale, delta=delta, borderType=cv2.BORDER_DEFAULT)
    grad_y = cv2.Sobel(I, ddepth, 0, 1, ksize=k, scale=scale, delta=delta, borderType=cv2.BORDER_DEFAULT)

    magnitude, orientaion = cv2.cartToPolar(grad_x, grad_y, angleInDegrees=1)

    # Quiver Plot
    res = 4
    N, M = I.shape
    X, Y = np.meshgrid(np.arange(0, M, res), np.arange(0, N, res))
    f = figure(2)
    quiver(X, Y, grad_x[::res,::res], grad_y[::res,::res])
    f.show()

    #magnitude = grad_x + grad_y

    cv2.imshow('dst', magnitude)
    return magnitude, orientaion

def findEllipseContour(img, gradientMagnitude, estimatedCenter, estimatedRadius, gradientRotation, nPts=20, useDirection=False, show=False):
    P = getCircleSamples(center=estimatedCenter, radius=estimatedRadius, nPoints=nPts)
    t=0;
    newPupil = np.zeros((nPts,1,2)).astype(np.float32)
    for (x,y,dx,dy) in P:
        normal_angle = np.arctan2(dx, dy) * (180 / math.pi)
        p = int(x),int(y)
        if show:
            cv2.circle(img, p, 1, (255,255,0), 2)
            cv2.line(img, estimatedCenter, p, (0,255,255), 1)

        p = findMaxGradientValueOnNormal(gradientMagnitude, estimatedCenter, p, useDirection, normal_angle, gradientRotation)
        p = (p[0],p[1])
        if show:
            cv2.circle(img, p, 1, (255,0,0), 2)
            cv2.line(img, estimatedCenter, p, (0,0,255), 1)
        newPupil[t] = p
        t += 1

    ellipse = cv2.fitEllipse(newPupil)
    if show:
        cv2.ellipse(img, ellipse, color=(255,0,255), thickness=2)
    return ellipse

def findMaxGradientValueOnNormal(gradientMagnitude,p1,p2, useDirection, direction, gradientRotation):
    pts = getLineCoordinates(p1, p2)
    normalVals = gradientMagnitude[pts[:, 1],pts[:, 0]]

    if useDirection:
        px, py, max, i = 0, 0, 0, 0
        print "#########"
        for p in pts:
            x,y = p
            diff = abs(direction - gradientRotation[y][x])
            if diff < 20 and normalVals[i] > max:
                max = normalVals[i]
                px, py = x, y
            i += 1
        if px > 0 and py > 0:
            max = np.argmax(normalVals)
            print pts[max]
            print px, py
            return px,py

    max = np.argmax(normalVals)

    return pts[max]

def update(I):
    '''Calculate the image features and display the result based on the slider values'''
    #global drawImg
    global frameNr, drawImg
    img = I.copy()
    sliderVals = getSliderVals()
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # Do the magic

    pupils = GetPupil(gray.copy(), sliderVals['pupilThr'], sliderVals['minSizePupil'], sliderVals['maxSizePupil'],
                      sliderVals['minExtendPupil'], sliderVals['maxExtendPupil'], sliderVals['minCirclePupil'],
                      sliderVals['maxCirclePupil'], sliderVals['pupilMorph'])
    glints = GetGlints(gray.copy(), sliderVals['glintThr'], sliderVals['minSizeGlint'], sliderVals['maxSizeGlint'],
                       sliderVals['glintMorph'])
    glints = FilterPupilGlint(pupils, glints, sliderVals['minDistance'], sliderVals['maxDistance'])

    gradientMaginitude, gradientRotation = getGradientImageInfo(gray)

    #iris = GetIrisUsingThreshold(gray.copy(), sliderVals['pupilThr'])
    #pupils = detectPupilKMeans(gray.copy(), 5, 30, sliderVals['minSizePupil'], sliderVals['maxSizePupil'],
                      #sliderVals['minExtendPupil'], sliderVals['maxExtendPupil'])

    #Do template matching
    global leftTemplate
    global rightTemplate

    if len(pupils) == 1:
        C = int(pupils[0][0][0]), int(pupils[0][0][1])
        eyecorners = GetEyeCorners(leftTemplate, rightTemplate, C)
    else:
        eyecorners = GetEyeCorners(leftTemplate, rightTemplate)

    #Display results
    global frameNr, drawImg
    x, y = 10, 10
    #setText(img,(x,y),"Frame:%d" %frameNr)

    # for non-windows machines we print the values of the threshold in the original image
    if sys.platform != 'win32':
        step = 18
        cv2.putText(img, "pupilThr :" + str(sliderVals['pupilThr']), (x, y + step), cv2.FONT_HERSHEY_PLAIN, 1.0,
                    (255, 255, 255), lineType=cv2.CV_AA)
        cv2.putText(img, "glintThr :" + str(sliderVals['glintThr']), (x, y + 2 * step), cv2.FONT_HERSHEY_PLAIN, 1.0,
                    (255, 255, 255), lineType=cv2.CV_AA)

    cv2.imshow('Result', img)

    #Uncomment these lines as your methods start to work to display the result in the
    #original image
    for pupil in pupils:
        cv2.ellipse(img, pupil, (0, 255, 0), 1)
        C = int(pupil[0][0]), int(pupil[0][1])
        cv2.circle(img, C, 2, (0, 0, 255), 4)

        # Use the average of the axes of the ellipse that fits the pupil, times 5. 5 since it seems to be a good match - could be set a little lower
        circleRadius = ((pupil[1][0]/2+pupil[1][1]/2)/2)*3;
        ellipse = findEllipseContour(img, gradientMaginitude, C, circleRadius, gradientRotation, show=True)

        newC = (int(ellipse[0][0]),int(ellipse[0][1]))
        newR = ((ellipse[1][0]/2+ellipse[1][1]/2)/2)*4
        ellipse = findEllipseContour(img, gradientMaginitude, newC, newR, gradientRotation, show=True, useDirection=False)

    for glint in glints:
        C = int(glint[0]), int(glint[1])
        cv2.circle(img, C, 2, (255, 0, 255), 5)

    #def circleTest():
        nPts = 20
        #C = (100,100)
        circleRadius = 40;
        P= getCircleSamples(center=C, radius=circleRadius, nPoints=nPts)
        t=0;
        for (x,y,dx,dy) in P:
            cv2.circle(img, (int(x),int(y)), 1, (255,0,0), 2)
            cv2.line(img, C, (int(x),int(y)), (0,0,255), 1)
            print (x,y,dx,dy)

    for eyecorner in eyecorners:
        cv2.rectangle(img, (eyecorner[0], eyecorner[1]), (eyecorner[2], eyecorner[3]), (255, 0, 0))
    #For Iris detection - Week 2
    #circularHough(gray)

    cv2.imshow('Result', img)
    drawImg = img.copy()


def printUsage():
    print "Q or ESC: Stop"
    print "SPACE: Pause"
    print "r: reload video"
    print 'm: Mark region when the video has paused'
    print 's: toggle video  writing'
    print 'c: close video sequence'


def run(fileName, resultFile='eyeTrackingResults.avi'):
    ''' MAIN Method to load the image sequence and handle user inputs'''
    global imgOrig, frameNr, drawImg
    setupWindowSliders()
    props = RegionProps();
    cap, imgOrig, sequenceOK = getImageSequence(fileName)
    videoWriter = 0

    frameNr = 0
    if (sequenceOK):
        update(imgOrig)
    printUsage()
    frameNr = 0;
    saveFrames = False

    while (sequenceOK):
        sliderVals = getSliderVals();
        frameNr = frameNr + 1
        ch = cv2.waitKey(1)
        #Select regions
        if (ch == ord('m')):
            if (not sliderVals['Running']):
                roiSelect = ROISelector(imgOrig)
                pts, regionSelected = roiSelect.SelectArea('Select left eye corner', (400, 200))
                if (regionSelected):
                    global leftTemplate
                    leftTemplate = imgOrig[pts[0][1]:pts[1][1], pts[0][0]:pts[1][0]]

                roiSelect = ROISelector(imgOrig)
                pts, regionSelected = roiSelect.SelectArea('Select right eye corner', (400, 200))
                if (regionSelected):
                    global rightTemplate
                    rightTemplate = imgOrig[pts[0][1]:pts[1][1], pts[0][0]:pts[1][0]]

        if ch == 27:
            break
        if (ch == ord('s')):
            if ((saveFrames)):
                videoWriter.release()
                saveFrames = False
                print "End recording"
            else:
                imSize = np.shape(imgOrig)
                videoWriter = cv2.VideoWriter(resultFile, cv2.cv.CV_FOURCC('D', 'I', 'V', '3'), 15.0,
                                              (imSize[1], imSize[0]), True)  #Make a video writer
                saveFrames = True
                print "Recording..."

        if (ch == ord('q')):
            break
        if (ch == 32):  #Spacebar
            sliderVals = getSliderVals()
            cv2.setTrackbarPos('Stop/Start', 'Threshold', not sliderVals['Running'])
        if (ch == ord('r')):
            frameNr = 0
            sequenceOK = False
            cap, imgOrig, sequenceOK = getImageSequence(fileName)
            update(imgOrig)
            sequenceOK = True

        sliderVals = getSliderVals()
        if (sliderVals['Running']):
            sequenceOK, imgOrig = cap.read()
            if (sequenceOK):  #if there is an image
                update(imgOrig)
            if (saveFrames):
                videoWriter.write(drawImg)

    cv2.destroyAllWindows()

    if (videoWriter != 0):
        videoWriter.release()
    print "Closing videofile..."


#------------------------


#------------------------------------------------
#   Methods for segmentation
#------------------------------------------------
def detectPupilKMeans(gray, K, distanceWeight, minSize, maxSize, minExtend, maxExtend, reSize=(40, 40)):
    ''' Detects the pupil in the image, gray, using k-means
        gray              : grays scale image
        K                 : Number of clusters
        distanceWeight    : Defines the weight of the position parameters
        reSize            : the size of the image to do k-means on
    '''
    #Resize for faster performance
    x, y = gray.shape
    smallI = cv2.resize(gray, (y / 14, x / 14))  #Keeping the format instead of standard resize
    M, N = smallI.shape
    #Generate coordinates in a matrix
    X, Y = np.meshgrid(range(M), range(N))

    #Make coordinates and intensity into one vectors
    z = smallI.flatten()
    x = X.flatten()
    y = Y.flatten()
    O = len(x)
    #make a feature vectors containing (x,y,intensity)
    features = np.zeros((O, 3))
    features[:, 0] = z
    features[:, 1] = y / distanceWeight  # Divide so that the distance of position weighs less than intensity
    features[:, 2] = x / distanceWeight
    features = np.array(features, 'f')
    # cluster data
    centroids, variance = kmeans(features, K)

    #use the found clusters to map
    label, distance = vq(features, centroids)
    # re-create image from
    labelIm = np.array(np.reshape(label, (M, N)))

    # Show the image for testing
    f = figure(1)
    imshow(labelIm)
    f.canvas.draw()
    f.show()

    #Create new blank img with same shape, and paint the "darkest" cluster white, therefor thresholding the image
    # source: http://en.wikipedia.org/wiki/K-means_clustering
    tmpLabel = np.zeros((labelIm.shape))
    tmpLabel[labelIm == np.argmin(centroids[:, 0])] = [255]

    #Convert the image to a binary image
    val, binI = cv2.threshold(np.array(cv2.resize(tmpLabel, (gray.shape[1], gray.shape[0])), dtype='uint8'), 0, 255, cv2.THRESH_BINARY)

    contours, hierarchy = cv2.findContours(binI, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    pupils = [];
    prop_calc = RegionProps()

    #Filter out Blobs
    for contour in contours:
        prop = prop_calc.CalcContourProperties(contour, ["centroid", "area", "extend", "Perimiter"])
        center, area, extend= prop["Centroid"], prop["Area"], prop["Extend"],
        circleProx = (prop.get("Perimiter") / (2 * np.sqrt(np.pi * prop.get('Area'))))
        if minSize < area < maxSize and minExtend < extend < maxExtend and 0.95 < circleProx < 1.65:
            ellipse = cv2.fitEllipse(contour)
            pupils.append(ellipse)

    cv2.imshow("Threshold", binI)
    return pupils


def detectPupilHough(gray):
    #Using the Hough transform to detect ellipses
    blur = cv2.GaussianBlur(gray, (31, 31), 11)
    ##Pupil parameters
    dp = 6;
    minDist = 10
    highThr = 30  #High threshold for canny
    accThr = 600;  #accumulator threshold for the circle centers at the detection stage. The smaller it is, the more false circles may be detected
    maxRadius = 70;
    minRadius = 20;
    #See help for http://opencv.itseez.com/modules/imgproc/doc/feature_detection.html?highlight=houghcircle#cv2.HoughCirclesIn thus
    circles = cv2.HoughCircles(blur, cv2.cv.CV_HOUGH_GRADIENT, dp, minDist, None, highThr, accThr, minRadius, maxRadius)
    #Print the circles
    gColor = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
    if (circles != None):
        #print circles
        all_circles = circles[0]
        M, N = all_circles.shape
        k = 1
        for c in all_circles:
            cv2.circle(gColor, (int(c[0]), int(c[1])), c[2], (int(k * 255 / M), k * 128, 0))
            K = k + 1
        #Circle with max votes
        c = all_circles[0, :]
        cv2.circle(gColor, (int(c[0]), int(c[1])), c[2], (0, 0, 255))
    cv2.imshow("hough", gColor)


#--------------------------
#         UI related
#--------------------------

def setText(dst, (x, y), s):
    cv2.putText(dst, s, (x + 1, y + 1), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 0), thickness=2, lineType=cv2.CV_AA)
    cv2.putText(dst, s, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), lineType=cv2.CV_AA)


def setupWindowSliders():
    ''' Define windows for displaying the results and create trackbars'''
    cv2.namedWindow("Result")
    cv2.namedWindow('Threshold')
    cv2.namedWindow("TempResults")
    cv2.namedWindow("Glint")
    cv2.namedWindow("KMean")


    #Threshold value for the pupil intensity
    cv2.createTrackbar('pupilThr', 'Threshold', 90, 255, onSlidersChange)
    #Threshold value for the glint intensities
    cv2.createTrackbar('glintThr', 'Glint', 240, 255, onSlidersChange)

    #define the minimum and maximum areas of the pupil
    cv2.createTrackbar('minSize', 'Threshold', 20, 200, onSlidersChange)
    cv2.createTrackbar('maxSize', 'Threshold', 200, 200, onSlidersChange)

    #define the minimum and maximum extend of the pupil
    cv2.createTrackbar('minExtend', 'Threshold', 1, 100, onSlidersChange)
    cv2.createTrackbar('maxExtend', 'Threshold', 100, 100, onSlidersChange)
    #define the minimum and maximum extend of the pupil
    cv2.createTrackbar('minCircle', 'Threshold', 100, 200, onSlidersChange)
    cv2.createTrackbar('maxCircle', 'Threshold', 200, 200, onSlidersChange)

    cv2.createTrackbar('Morphing', 'Threshold', 1, 40, onSlidersChange)
    cv2.createTrackbar('Morphing', 'Glint', 1, 40, onSlidersChange)

    cv2.createTrackbar('minSize', 'Glint', 20, 1000, onSlidersChange)
    cv2.createTrackbar('maxSize', 'Glint', 800, 1000, onSlidersChange)
    cv2.createTrackbar('minExtend', 'Glint', 1, 600, onSlidersChange)
    cv2.createTrackbar('maxExtend', 'Glint', 600, 600, onSlidersChange)
    cv2.createTrackbar('minDistance', 'Glint', 1, 200, onSlidersChange)
    cv2.createTrackbar('maxDistance', 'Glint', 200, 200, onSlidersChange)
    #Value to indicate whether to run or pause the video
    cv2.createTrackbar('Stop/Start', 'Threshold', 0, 1, onSlidersChange)
    cv2.createTrackbar('Stop/Start', 'Glint', 0, 1, onSlidersChange)
    cv2.createTrackbar('distanceWeight', 'Threshold', 2, 50, onSlidersChange)
    cv2.createTrackbar('kVal', 'Threshold', 2, 50, onSlidersChange)


def getSliderVals():
    '''Extract the values of the sliders and return these in a dictionary'''
    sliderVals = {}
    sliderVals['Running'] = 1 == cv2.getTrackbarPos('Stop/Start', 'Threshold') or 1 == cv2.getTrackbarPos('Stop/Start',
                                                                                                          'Glint')
    sliderVals['pupilThr'] = cv2.getTrackbarPos('pupilThr', 'Threshold')

    sliderVals['glintThr'] = cv2.getTrackbarPos('glintThr', 'Glint')
    sliderVals['minSizePupil'] = 50 * cv2.getTrackbarPos('minSize', 'Threshold')
    sliderVals['maxSizePupil'] = 50 * cv2.getTrackbarPos('maxSize', 'Threshold')
    sliderVals['minExtendPupil'] = cv2.getTrackbarPos('minExtend', 'Threshold') / 100.0
    sliderVals['maxExtendPupil'] = cv2.getTrackbarPos('maxExtend', 'Threshold') / 100.0
    sliderVals['minCirclePupil'] = cv2.getTrackbarPos('minCircle', 'Threshold') / 100.0
    sliderVals['maxCirclePupil'] = cv2.getTrackbarPos('maxCircle', 'Threshold') / 100.0
    sliderVals['minSizeGlint'] = cv2.getTrackbarPos('minSize', 'Glint')
    sliderVals['maxSizeGlint'] = cv2.getTrackbarPos('maxSize', 'Glint')
    sliderVals['minDistance'] = cv2.getTrackbarPos('minDistance', 'Glint')
    sliderVals['maxDistance'] = cv2.getTrackbarPos('maxDistance', 'Glint')
    sliderVals['pupilMorph'] = cv2.getTrackbarPos('Morphing', 'Threshold')
    sliderVals['glintMorph'] = cv2.getTrackbarPos('Morphing', 'Glint')
    sliderVals['DistanceWeight'] = cv2.getTrackbarPos('distanceWeight', 'Threshold')
    sliderVals['KVal'] = cv2.getTrackbarPos('kVal', 'Threshold')
    return sliderVals


def onSlidersChange(dummy=None):
    ''' Handle updates when slides have changed.
     This  function only updates the display when the video is put on pause'''
    global imgOrig;
    sv = getSliderVals()
    if (not sv['Running']):  # if pause
        update(imgOrig)

#--------------------------
#         main
#--------------------------
run(inputFile)